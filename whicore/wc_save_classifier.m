function wc_save_classifier(src, dst, type)

    if nargin < 3
        type = 'cnbi-gaussian';
    end

    if ischar(src) 
        srcst = load(src);
    elseif isstruct(src)
        srcst = src;
    end
    
    fid = fopen(dst, 'w');
    if(fid < 0)
        error('chk:file', 'Invalid destination path');
    end
    
    switch lower(type)
        case 'cnbi-gaussian'
            disp('Saving cnbi gaussian classifier to binary'); 
            write_gaussian_cnbi(fid, srcst);
 
        
        otherwise
            error('chk:typ', 'Classifier type not recognized');
    end
        
    
    fclose(fid);
    
end


function write_gaussian_cnbi(fid, srcst)
    
    format   = 'WHITK';
    version  = '1.0';
    type     = 'classifier';
    label    = 'cnbi-gaussian';
    
    subject  = srcst.analysis.info.subject;
    
    nclasses    = size(srcst.analysis.tools.net.gau.M, 1);
    classlbs    = srcst.analysis.settings.task.classes_old;
    nprototypes = size(srcst.analysis.tools.net.gau.M, 2);
    nfeatures   = size(srcst.analysis.tools.net.gau.M, 3);
    
    shrdcov = srcst.analysis.settings.classification.gau.sharedcov;
    mimean  = srcst.analysis.settings.classification.gau.mimean;
    micov   = srcst.analysis.settings.classification.gau.micov;

    [idchan, idfreq] = getfeatures_gaussian_cnbi(srcst.analysis.tools.features);
    
    M = permute(srcst.analysis.tools.net.gau.M, [3 2 1]); 
    C = permute(srcst.analysis.tools.net.gau.C, [3 2 1]); 
    
    
    wc_writeheader(fid, format, version, type, label);
    wc_writestring(fid, subject);
    
    wc_writebyte(fid, nclasses, 'uint32');
    wc_writevector(fid, classlbs, 'uint32');
    wc_writebyte(fid, nprototypes, 'uint32');
    wc_writebyte(fid, nfeatures, 'uint32');
    
    wc_writebyte(fid, shrdcov, 'char');
    wc_writebyte(fid, mimean, 'float');
    wc_writebyte(fid, micov, 'float');
    
    wc_writevector(fid, idchan, 'uint32');
    wc_writevector(fid, idfreq, 'uint32');
    
    wc_writeeigen(fid, M, 'double');
    wc_writeeigen(fid, C, 'double');
    
end

function [chans, freqs] = getfeatures_gaussian_cnbi(strfeatures)
    
    chans = [];
    freqs = [];
    chIndex = strfeatures.channels;
    for chId = 1:length(chIndex)
        currChId = chIndex(chId);
        freqIndex = strfeatures.bands{currChId};
       
        chans = cat(1, chans, repmat(currChId, length(freqIndex), 1));
        freqs = cat(1, freqs, freqIndex');
    end


end

function P = getdata_gaussian_cnbi(D) 
    P = [];
    for sId = 1:size(D, 3)
        P = cat(1, P, D(:, :, sId));
    end
end
